﻿
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
//using System.Globalization;
//using System.IO;
namespace LOLAnalyze.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> index()
        {
            TempData["lang"] = "";
            TempData["version"] = "";
            TempData["remote"] = "";
            string langUrl = "https://ddragon.leagueoflegends.com/cdn/languages.json";
            string versionUrl = "https://ddragon.leagueoflegends.com/api/versions.json";

            HttpClient client = new HttpClient();
            string responseLang = await client.GetStringAsync(langUrl);

            HttpResponseMessage response = await client.GetAsync(versionUrl);
            string responseVersion = await response.Content.ReadAsStringAsync();

            //string path = $"{Server.MapPath("json").Replace("Home\\", "")}\\{TempData["lang"]}_hero.json";
            if (response.IsSuccessStatusCode)
            {

                List<SelectListItem> langItems = new List<SelectListItem>();
                List<SelectListItem> versionItems = new List<SelectListItem>();
                ViewBag.langs = langItems;
                ViewBag.versions = versionItems;
                string[] langs = JsonConvert.DeserializeObject<string[]>(responseLang);
                string[] versions = JsonConvert.DeserializeObject<string[]>(responseVersion);
                foreach (string lang in langs) { langItems.Add(new SelectListItem { Text = lang}); };
                foreach (string version in versions) { versionItems.Add(new SelectListItem { Text = version }); };
                ViewBag.langs = langItems;
                ViewBag.versions = versionItems;
                //StreamWriter writer = new StreamWriter(path, false, System.Text.Encoding.UTF8);
                // writer.WriteLine($" 'version' :{ViewBag.version}, 'lang' : {ViewBag.lang}");
                // writer.Close();
            }
            else
            {
                ViewBag.Message = "無法存取來源端";
            }
            return View();
        }
        [HttpPost]
        public ActionResult set(string lang, string version)
        {
            string remote = "http://ddragon.leagueoflegends.com/cdn";
            Environment.SetEnvironmentVariable("lang", lang);
            Environment.SetEnvironmentVariable("version", version);
            Environment.SetEnvironmentVariable("remote", remote);
            TempData["lang"] = lang;
            TempData["version"] = version;
            TempData["remote"] = remote;
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


    }
}