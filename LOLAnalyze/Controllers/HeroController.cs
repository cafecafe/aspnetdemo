﻿using System;
using System.Linq;

using System.Web.Mvc;
using LOLAnalyze.Models;
using System.Threading.Tasks;
using LOLAnalyze.Service;

using System.Web.Routing;
using System.Data.Entity.Migrations;
using System.Collections.Generic;

namespace LOLAnalyze.Controllers
{
    public class HeroController : Controller
    {
        DatabaseEntities db = new DatabaseEntities();
        // GET: Hero
        public ActionResult Index(int order = 0)
        {
            var heros  = new List<Heros>();
            switch (order)
            {
                case 1: 
                    heros = db.Heros.OrderByDescending(m => m.Attack).ToList();
                    break;
                case 2:
                    heros = db.Heros.OrderByDescending(m => m.Defense).ToList();
                    break;
                case 3:
                    heros = db.Heros.OrderByDescending(m => m.Magic).ToList();
                    break;
                case 4:
                    heros = db.Heros.OrderByDescending(m => m.Difficulty).ToList();
                    break;
                case 5:
                    heros = db.Heros.OrderByDescending(m => m.Magic + m.Attack - m.Defense).ToList();
                    break;
                case 6:
                    heros = db.Heros.OrderByDescending(m =>  -m.Magic + m.Attack + m.Defense).ToList();
                    break;
                case 7:
                    heros = db.Heros.OrderByDescending(m => m.Magic - m.Attack + m.Defense).ToList();
                    break;
                case 8:
                    heros = db.Heros.OrderBy(m => 
                        Math.Abs((m.Magic + m.Attack + m.Defense)/3 - m.Magic) +
                        Math.Abs((m.Magic + m.Attack + m.Defense) / 3 - m.Attack) +
                        Math.Abs((m.Magic + m.Attack + m.Defense) / 3 - m.Defense) 
                    ).ToList();
                    break;
                default:
                    heros = db.Heros.ToList();
                    break;

            }
            ViewBag.ImgUrl = new string[heros.Count()];
            foreach (Heros hero in heros)
            {
                ViewBag.ImgUrl[hero.Id] = $"{TempData["remote"] }/{TempData["version"]}/img/champion/{hero.IDName.Replace(" ", String.Empty)}.png";
            }
            return View(heros);
        }

        // GET: Hero/Details/5
        public ActionResult Details(int id)
        {
            Heros hero = db.Heros.Find(id);
            string name = hero.IDName.Replace(" ", String.Empty);
            ViewBag.imgUrl = $"http://ddragon.leagueoflegends.com/cdn/img/champion/splash/{name}_0.jpg";
            if (hero == null)
            {
                return HttpNotFound();
            }
            return View(hero);
        }

        // GET: Hero/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Hero/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch{
                return View();
            }
        }

        // GET: Hero/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Hero/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Hero/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Hero/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public async Task<ActionResult> Sync()
        {
           
            try  
            {
                JsonService request = new JsonService("champion");
                var data = await request.makeRequest<Heros>();
                var dataSet = new RouteValueDictionary(data["data"]);
                foreach(var item in dataSet)
                {
                    Heros hero = new Heros();
                    var row = (dynamic)item.Value;
                    hero.Name = row["name"];
                    hero.IDName = row["id"];
                    hero.Title = row["title"];
                    hero.Blurb = row["blurb"];
                    hero.Attack = row["info"]["attack"];
                    hero.Defense = row["info"]["defense"];
                    hero.Magic = row["info"]["magic"];
                    hero.Difficulty = row["info"]["difficulty"];
                    db.Heros.AddOrUpdate(m => m.IDName, hero);
                }
                db.SaveChanges();
            }
            catch(Exception ex)
            {
                ViewBag.Content = "資料同步錯誤";
            }
            return RedirectToAction("Index");
        }
    }
}
