﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Data.Entity;
using System;

namespace LOLAnalyze.Service
{
    public class JsonService
    {
        private string url;
        public JsonService(string type )
        {
           
            this.url =$"{ Environment.GetEnvironmentVariable("remote") }" +
                $"/{Environment.GetEnvironmentVariable("version") }/data/{Environment.GetEnvironmentVariable("lang")}/{type}.json";
            Environment.SetEnvironmentVariable("url", this.url);
        }
        public async Task<dynamic> makeRequest<T>(string req = "")
        {
            HttpClient client = new HttpClient();
            string response = await client.GetStringAsync(this.url);
            return JsonConvert.DeserializeObject<dynamic>(response);

        }
    }
}