﻿CREATE TABLE [dbo].[Heros] (
    [Id]         INT         NOT NULL,
    [Name]       NCHAR (10)  NULL,
    [Title]      NCHAR (10)  NULL,
    [Blurb]      NCHAR (255) NULL,
    [IDName]     NCHAR (10)  NULL,
    [Attack]     INT         NOT NULL,
    [Defense]    INT         NOT NULL,
    [Magic]      INT         NOT NULL,
    [Difficulty] INT         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);